SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de données :  `i-garden`
--
USE `i-garden`;
--
-- Déchargement des données de la table `cycle_vegetatif`
--

INSERT INTO `cycle_vegetatif` (`id_cycle_vegetatif`, `nom`) VALUES
(2, 'Croissance'),
(3, 'Floraison'),
(4, 'Fructification'),
(1, 'Germination'),
(5, 'Mort');

--
-- Déchargement des données de la table `statut`
--

INSERT INTO `statut` (`id_statut`, `nom`) VALUES
(1, 'Actif'),
(2, 'Passé'),
(3, 'Traité');

--
-- Déchargement des données de la table `etat`
--

INSERT INTO `etat` (`id_etat`, `nom`, `id_statut`) VALUES
(1, 'Malade', 1),
(2, 'Assoiffé', 1);

--
-- Déchargement des données de la table `type_taxon`
--

INSERT INTO `type_taxon` (`id_type_taxon`, `nom`) VALUES
(1, 'Famille'),
(2, 'Genre'),
(3, 'Espèce');


--
-- Déchargement des données de la table `taxon`
--

INSERT INTO `taxon` (`id_taxon`, `id_type_taxon`, `id_parent`, `nom`) VALUES
(1, 1, NULL, 'Cucurbitaceae'),
(2, 1, NULL, 'Alliaceae'),
(3, 2, 1, 'Cucurbita'),
(4, 2, 2, 'Allium'),
(5, 3, 3, 'Cucurbita pepo'),
(6, 3, 4, 'Bleu de Solaise');


--
-- Déchargement des données de la table `critere_rusticite`
--

INSERT INTO `critere_rusticite` (`id_critere_rusticite`, `code`, `temperature_min`, `temperature_max`) VALUES
(1, '1', '-99.0', '-45.0'),
(2, '2', '-45.0', '-40.0'),
(3, '3', '-40.0', '-34.0'),
(4, '4', '-34.0', '-29.0'),
(5, '5', '-29.0', '-23.0'),
(6, '6', '-23.0', '-18.0'),
(7, '7', '-18.0', '-12.0'),
(8, '8', '-12.0', '-7.0'),
(9, '8a', '-12.2', '-9.4'),
(10, '8b', '-9.4', '-6.7'),
(11, '9', '-7.0', '-1.0'),
(12, '9a', '-6.7', '-3.9'),
(13, '9b', '-3.9', '-1.1'),
(14, '10', '-1.0', '4.0'),
(15, '11', '4.0', '99.0');


--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_utilisateur`, `pseudo`, `email`, `mot_de_passe`) VALUES
(1, 'toto', 'toto@toto.fr', 'f71dbe52628a3f83a77ab494817525c6'),
(2, 'igor', 'igor@toto.fr', 'dd97813dd40be87559aaefed642c3fbb');

--
-- Déchargement des données de la table `code_postal`
--

INSERT INTO `code_postal` (`id_code_postal`, `valeur`) VALUES
(1, 35000),
(2, 35170);

--
-- Déchargement des données de la table `jardin`
--

INSERT INTO `jardin` (`id_jardin`, `id_critere_rusticite`, `nom`, `id_code_postal`) VALUES
(1, 11, 'Jardin de toto', 1),
(2, 10, 'Jardin partagé', 2);

--
-- Déchargement des données de la table `utilisateur-jardin`
--

INSERT INTO `utilisateur-jardin` (`id_utilisateur`, `id_jardin`) VALUES
(1, 1),
(1, 2),
(2, 2);

--
-- Déchargement des données de la table `type_sol`
--

INSERT INTO `type_sol` (`id_type_sol`, `name`) VALUES
(1, 'terre');

--
-- Déchargement des données de la table `zone`
--

INSERT INTO `zone` (`id_zone`, `id_jardin`, `id_critere_rusticite`, `nom`, `description`, `est_special`) VALUES
(1, 1, NULL, 'Carré potager', 'Terre du jardin, terreau et compost', 0),
(2, 1, 13, 'Veranda', NULL, 0),
(3, 2, NULL, 'Jardin partagé', 'Terre du jardin + Compost + Ammendé en 2021', 0);

--
-- Déchargement des données de la table `classification`
--

INSERT INTO `classification` (`id_classification`, `name`) VALUES
(1, 'Vivace'),
(2, 'Annuelle');

--
-- Déchargement des données de la table `graine`
--

INSERT INTO `graine` (`id_graine`, `id_taxon`, `nom`, `nom_latin`, `id_classification`, `hauteur_max`, `photo`, `icone`, `description`) VALUES
(1, 6, 'Poireau bleu de solaise', 'Allium porrum', 1, 75, NULL, NULL, NULL),
(2, 5, 'Courge spaghetti', 'Cucurbita pepo', 2, 50, NULL, NULL, NULL);


--
-- Déchargement des données de la table `plante`
--

INSERT INTO `plante` (`id_plante`, `id_graine`, `id_zone`, `id_cycle_vegetatif`, `date_semis`, `date_repiquage`, `date_floraison`, `date_recolte`, `qte_recolte`) VALUES
(5, 1, 3, 2, NULL, '2022-02-08', NULL, NULL, NULL),
(6, 2, 1, 2, '2022-04-02', '2022-05-01', NULL, NULL, NULL);

--
-- Déchargement des données de la table `plante-etat`
--

INSERT INTO `plante-etat` (`id_plante`, `id_etat`, `date`) VALUES
(6, 1, '2022-04-02 00:00:00');

--
-- Déchargement des données de la table `histo_plante-cycle_vegetatif`
--

INSERT INTO `histo_plante-cycle_vegetatif` (`id_cycle_vegetatif`, `id_plante`, `date_changement`) VALUES
(1, 6, '2022-04-02 00:00:00');

--
-- Déchargement des données de la table `histo_plante-etat`
--

INSERT INTO `histo_plante-etat` (`id_etat`, `id_plante`, `date_changement`) VALUES
(2, 6, '2022-04-27 00:00:00'),
(2, 6, '2022-05-01 00:00:00');

--
-- Déchargement des données de la table `historique_zone`
--

INSERT INTO `histo_plante-zone` (`id_plante`, `id_zone`, `date_changement`) VALUES
(6, 2, '2022-05-01 00:00:00');

COMMIT;

