use `i-garden`;

-- ----------------
-- Transaction
-- ----------------

-- -----------------------------------------------------
-- Ajout des colonnes latitude, longitude, largeur et hauteur à la table zone
-- -----------------------------------------------------

SET autocommit = OFF;
START TRANSACTION;
    ALTER TABLE `zone`
    	ADD `latitude` DECIMAL(12,6) NULL,
    	ADD `longitude` DECIMAL(12,6) NULL,
    	ADD `largeur` INT(12) UNSIGNED NULL,
    	ADD `hauteur` INT(12) UNSIGNED NULL;
COMMIT;
SET autocommit = ON;