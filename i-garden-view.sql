use `i-garden`;
-- ----------------
-- Views
-- ----------------

-- -----------------------------------------------------
-- Vue Liste_des_plantes contenant : Le nom de la plante, le nom de la zone
-- où elle se trouve, son cycle végétatif actuel
-- -----------------------------------------------------
DROP VIEW IF EXISTS Liste_des_plantes;
CREATE VIEW Liste_des_plantes AS
    SELECT
        g.nom AS 'nom graine',
        z.nom AS 'nom zone',
        cv.nom AS 'cycle vegetatif'
    FROM
        `plante` p,
        `graine` g,
        `zone` z,
        `cycle_vegetatif` cv
    WHERE
        g.`id_graine` = p.`id_graine`
        AND z.`id_zone` = p.`id_zone`
        AND cv.`id_cycle_vegetatif` = p.`id_cycle_vegetatif`;


-- -----------------------------------------------------
-- Vue  Zones_recap contenant : Liste des zones, Nom de la zone
-- code rusticité, nombre de plantes affectées à la zone
-- -----------------------------------------------------
DROP VIEW IF EXISTS Zones_recap;
CREATE VIEW Zones_recap AS
    SELECT
        z.`nom` AS 'nom zone',
        cr.`code` AS 'code rusticite',
        COUNT(p.`id_zone`) AS 'nb plantes affectees'
    FROM
        `zone` z
    LEFT JOIN `critere_rusticite` cr
        ON cr.`id_critere_rusticite` = z.`id_critere_rusticite`
    LEFT JOIN `plante` p
    	ON p.`id_zone` = z.`id_zone`
    GROUP BY
    	p.`id_zone`;

 -- « Plantes_hors_zone » : liste des plantes qui ne sont affectées à aucune zone :
-- nom de la plante & propriétaire s’il y en a un.
DROP VIEW IF EXISTS `plantes_hors_zone`;
CREATE VIEW `plantes_hors_zone` AS
select g.`nom`, `id_utilisateur`
from `plante` p
    left join `graine` g on p.`id_graine` = g.`id_graine`
    left join `utilisateur-plante` `u-p` on p.id_plante = `u-p`.`id_plante`
where p.`id_zone` is null
;

-- Plantes_recap » Liste des graines :
-- Nom
-- Genre + espèce (issue de la famille associée) – Le genre doit commencer par une
-- minuscule, tout le reste écrit en minuscule
-- Nom latin
DROP VIEW IF EXISTS `plantes-recap`;
create view `plantes-recap` as
select
    g.`nom`,
    g.nom_latin as nom_latin,
    CONCAT(t0.nom, LOWER(t.nom)) as genre_espece
from
    `plante` p
    left join `graine` g on p.`id_graine` = g.`id_graine`
    left join `taxon` t on g.`id_taxon` = t.`id_taxon`
    left join `taxon` t0 on t.`id_parent` = t0.`id_taxon`
;
