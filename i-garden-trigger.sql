use `i-garden`;
-- ----------------
-- Triggers
-- ----------------

-- -----------------------------------------------------
-- Le changement de statut d’un état d’une plante doit être historisé.
-- -----------------------------------------------------
DROP TRIGGER IF EXISTS trigger_histo_etat;
DELIMITER |
CREATE TRIGGER trigger_histo_etat
BEFORE UPDATE
ON `plante-etat` FOR EACH ROW
BEGIN
    IF OLD.`id_plante` IS NOT NULL 
    AND OLD.`id_etat` IS NOT NULL
        THEN
            INSERT INTO `histo_plante-etat` (`id_plante`, `id_etat`, `date`, `date_changement`)
                SELECT `id_plante`, `id_etat`, `date`, NOW() FROM `plante-etat` WHERE `id_plante` = OLD.`id_plante`;
    END IF;
END |
DELIMITER ;


-- -----------------------------------------------------
-- Le changement de cycle végétatif doit être historisé.
-- -----------------------------------------------------
DROP TRIGGER IF EXISTS trigger_histo_cycle_vegetatif;
DELIMITER |
CREATE TRIGGER trigger_histo_cycle_vegetatif
BEFORE UPDATE
ON `plante` FOR EACH ROW
BEGIN
    IF OLD.`id_cycle_vegetatif` != NEW.`id_cycle_vegetatif`
        THEN
            INSERT INTO `histo_plante-cycle_vegetatif` (`id_plante`, `id_cycle_vegetatif`, `date_changement`)
                SELECT `id_plante`, `id_cycle_vegetatif`, NOW() FROM `plante` WHERE `id_plante` = OLD.`id_plante`;
    END IF;
END |
DELIMITER ;

-- -----------------------------------------------------
-- Une plante peut changer de zone, en quel cas ce changement doit être historisé
-- -----------------------------------------------------
DROP TRIGGER IF EXISTS trigger_histo_zone;
DELIMITER |
create trigger trigger_histo_zone
    before update
    on plante
    for each row
begin
    if OLD.id_zone != NEW.id_zone
    then
        insert into `histo_plante-zone`(id_plante, id_zone, date_changement)
            values (OLD.id_plante, OLD.id_zone, now());
    end if;
end |
DELIMITER ;

-- -----------------------------------------------------
-- La suppression d’une plante doit supprimer les historiques associés
-- -----------------------------------------------------
DROP TRIGGER IF EXISTS trigger_suppression_plante;
DELIMITER |
CREATE TRIGGER trigger_suppression_plante
BEFORE DELETE
ON `plante`
FOR EACH ROW
BEGIN
    DELETE FROM `plante-etat` WHERE `plante-etat`.`id_plante` LIKE OLD.`id_plante`;
    DELETE FROM `histo_plante-etat` WHERE `histo_plante-etat`.`id_plante` LIKE OLD.`id_plante`;
    DELETE FROM `histo_plante-zone` WHERE `histo_plante-zone`.`id_plante` LIKE OLD.`id_plante`;
    DELETE FROM `histo_plante-cycle_vegetatif` WHERE `histo_plante-cycle_vegetatif`.`id_plante` LIKE OLD.`id_plante`;
END |
DELIMITER ;

-- -----------------------------------------------------
-- La suppression d’une zone ne doit pas supprimer les plantes associées cependant une référence
-- à l’utilisateur détenteur de cette zone doit tout de même persister
-- -----------------------------------------------------
DROP TRIGGER IF EXISTS trigger_suppression_zone;
DELIMITER |
create trigger trigger_suppression_zone
    before delete
    on zone
    for each row
begin
    insert into `utilisateur-plante` (id_utilisateur, id_plante)
    values (
            (select id_plante from plante where plante.id_zone like OLD.id_zone),
            (select id_utilisateur from `utilisateur-jardin` where `utilisateur-jardin`.id_utilisateur like OLD.id_jardin)
           );
    delete from `histo_plante-zone` where `histo_plante-zone`.id_zone like old .id_zone;
end |
DELIMITER ;

-- ...
